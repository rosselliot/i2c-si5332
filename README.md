**Linux Kernel Module for the Silicon Labs Si5332 digital PLL synthesiser**

**To compile the kernel module**

Setup the environment for cross-compilation:

```
. /opt/fpgaioc/xilinx-standalone/environment-setup-cortexa9t2hf-neon-poky-linux-gnueabi
```

Call make, passing the path to KERNEL_SRC:

```
make KERNEL_SRC=/opt/fpgaioc/xilinx-standalone/sysroots/cortexa9t2hf-neon-poky-linux-gnueabi/lib/modules/4.14.0-fpgaioc/source
```

**Devicetree binding (ex. if device is at chip address 0x6A):**

    si5332@6a {
        compatible = "linux,si5332";
        reg = <0x6a>;     
    };


**Default clock configuration**

On loading the module, the Si5332 device is probed, and initially programmed to 
generate the following clock configuration:
 *  OUT0 - 100 MHz
 *  OUT1 - UNUSED
 *  OUT2 - 100 MHz
 *  OUT3 - 88.0519480519 MHz
 *  OUT4 - 88.0519480519 MHz


**Reprogramming the clock outputs of the PLL**

The clock signals can be reprogrammed via the provided utility function, si5332_config.
You must provide the utility with a C header file generated from the Silicon Labs
ClockBuilder Pro software:
    
```
$ sudo ./si5332_config -s si5332_regs_22_100_88_88.h
Reading si5332 clock configuration from file: si5332_regs_22_100_88_88.h
Configuring si5332 to custom clock configuration:
si5332 2-006a: Begin read of custom register configuration
si5332 2-006a: All data transferred. Initialising PLL...
si5332 2-006a: Configuring OUT0 to 22.013125 MHz
si5332 2-006a: Configuring OUT1 to UNUSED
si5332 2-006a: Configuring OUT2 to 100 MHz
si5332 2-006a: Configuring OUT3 to 88.0525 MHz
si5332 2-006a: Configuring OUT4 to 88.0525 MHz
si5332 2-006a: Wrote 69 configuration registers
```

The current clock configuration can also be read back using the utility, si5332_config:

```
$ sudo ./si5332_config -g
si5332 2-006a: Current clock configuration:
si5332 2-006a: OUT0 @ 100 MHz
si5332 2-006a: OUT1 @ UNUSED
si5332 2-006a: OUT2 @ 100 MHz
si5332 2-006a: OUT3 @ 88.0519480519 MHz
si5332 2-006a: OUT4 @ 88.0519480519 MHz
```   


    
