#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <fcntl.h>
#include <unistd.h>

#define MAX_NUM_REGS  255
#define NUM_OUTPUTS   5
#define SYSFS_DEVICE  "/sys/bus/i2c/drivers/si5332/2-006a/si5332"
#define WR_STRING_LEN 15
typedef struct
{
    unsigned int address; /* 8-bit register address */
    unsigned char value;  /* 8-bit register data */
} si5332_revd_register_t;

typedef struct
{
    char *name;
    char *freq;
} si5332_output_config_t;

int si5332_get_num_regs_from_file(char *line);

int si5332_get_num_regs_from_file(char *line) {
    
    int num_regs = -1;   
    char *token, *prevToken; 
    char *delim = "\t ";
    
    /* The number of registers is the last token */
    token = strtok(line, delim);
    while(token != NULL) {
        prevToken = token;
        token = strtok(NULL, delim);
    }
    /* Check we can convert the token to an integer */
    num_regs = atoi(prevToken);
    if (num_regs <= 0) {
        if (line)
            free(line);
        return -2;
    } 

    return num_regs;
}

unsigned int si5332_get_register_from_file(char * line, unsigned int regCount, si5332_revd_register_t *si5332_revd_registers) {

    char *token, *prevToken, *ptr, *delim = " "; 
    bool foundAddr = false;
    
    token = strtok(line, delim);
    while(token != NULL) {
        prevToken = token;
        /* address and values are in hex */
        if (strstr(prevToken, "0x") > 0) {
            /* Address is first... */
            if (!foundAddr) {
                si5332_revd_registers[regCount].address = strtol(prevToken, &ptr, 16);
                foundAddr = true;
            } else { /* Then value... */
                si5332_revd_registers[regCount].value = strtol(prevToken, &ptr, 16);
                break;
            }
        }
        token = strtok(NULL, delim);
    }
    
    return (++regCount);
}

unsigned int si5332_get_clock_config_from_file(char * line, bool *foundOutputs, 
                unsigned int outCount, si5332_output_config_t *si5332_clocks) {

    char *ret, *tok, *prevToken, *ptr, *delim = " "; 
    bool used = false;
    
    if (outCount >= NUM_OUTPUTS) {
        foundOutputs = false; 
        return outCount;
     }

    /* allocate memory for strings to be used with snprintf */
    char *outstr = malloc(sizeof(char) * 25);
    char *namestr = malloc(sizeof(char) * 5);
    
    /* Form name string */
    snprintf(namestr, 5,  "OUT%d", outCount);
    /* Assign to struct array */           
    si5332_clocks[outCount].name = namestr;
    
    delim = " :*,";
    tok = strtok(line, delim);
    while(tok != NULL) {
        if (strstr(tok, "Hz") != NULL) {
            snprintf(outstr, 25, "%s %s", prevToken, tok);
            si5332_clocks[outCount].freq = outstr;
            used = true;
            break;
        }
        prevToken = tok;
        tok = strtok(NULL, delim);
    }
    
    /* Clock output is unused */
    if (!used) {
       si5332_clocks[outCount].freq = "UNUSED";
    }       

    /* Return incremented count */
    return (++outCount);
}

int si5332_get_config_from_file(char * fileName, si5332_revd_register_t *si5332_revd_registers, si5332_output_config_t *si5332_clocks) {
    
    FILE *fHandle;
    char *line;
    size_t len = 0;
    ssize_t read;
    int num_regs = -1;
    bool foundOutputs = false;
    unsigned int regCount = 0, outCount = 0;

    fHandle = fopen(fileName, "r");
    if (fHandle == NULL) {
        return -1;
    } else {
        printf("Reading si5332 clock configuration from file: %s\n", fileName);
        while ((read = getline(&line, &len, fHandle)) != -1) {
            /* Get the number of registers to configure */
            if(strstr(line, "#define SI5332-GM1_REVD_REG_CONFIG_NUM_REGS") > 0) {
                num_regs = si5332_get_num_regs_from_file(line);
            }
            /* Find a register definition, i.e. {0x0006, 0x02 }, */
            if(strstr(line, "{ 0x00") > 0) {
                regCount = si5332_get_register_from_file(line, regCount, si5332_revd_registers);
            }
        
            /* Find the configuration comments, so we know what the config is */
            if(strstr(line, " * Outputs:") > 0) {
                foundOutputs = true;
            }

            if (foundOutputs == true) {
                if(strstr(line, " *    OUT") > 0) {
                   outCount = si5332_get_clock_config_from_file(line, &foundOutputs, outCount, si5332_clocks);
                }       
            }
        }
    }   

    fclose(fHandle);
    if (line)
        free(line);

    return num_regs;
}

int write_value(char *buf, size_t nBytes) {
    
    int ret, fd;

    fd = open(SYSFS_DEVICE, O_WRONLY);
    if(fd < 0)
        return fd;

    ret = write(fd, buf, nBytes);
    if (ret < 0 ) {
        printf("ERROR: failed to write '%s' to sysfs interface\n", buf);
    } else {
    //    printf("Wrote '%s' as %d bytes\n", buf,  ret);
    }
    close(fd);
    return ret;
}     

int si5332_write_to_sysfs(int num_regs, si5332_revd_register_t *si5332_revd_registers, si5332_output_config_t *si5332_clocks, bool isDefault) {
   
    int n = 0;
    char wrStr[WR_STRING_LEN];

    if(isDefault) {
        printf("Configuring si5332 to default clock configuration:\n");
        write_value("DEFAULT", 7);
    } else {
        printf("Configuring si5332 to custom clock configuration:\n");
        /* Send custom identifier */
        write_value("CUSTOM", 6);
        /* Send number of registers to write */
        snprintf(wrStr, WR_STRING_LEN, "%d", num_regs);
        write_value(wrStr, WR_STRING_LEN);
        /* Send register address, then register value, for all registers */
        for(n=0; n<num_regs; n++) {
            snprintf(wrStr, WR_STRING_LEN, "%x", si5332_revd_registers[n].address);
            write_value(wrStr, WR_STRING_LEN);
            snprintf(wrStr, WR_STRING_LEN, "%x", si5332_revd_registers[n].value);
            write_value(wrStr, WR_STRING_LEN);
        }
        /* Send clock name, then frequency value, for all clocks */
        for(n=0; n<NUM_OUTPUTS; n++) {
            snprintf(wrStr, WR_STRING_LEN, "%s", si5332_clocks[n].name);
            write_value(wrStr, WR_STRING_LEN);
            snprintf(wrStr, WR_STRING_LEN, "%s", si5332_clocks[n].freq);
            write_value(wrStr, WR_STRING_LEN);
        }
    }
}

int si5332_read_from_sysfs() {
   
    int fHandle;
    ssize_t bytes_read;
    char *buf;

    fHandle = open(SYSFS_DEVICE, O_RDONLY);
    if(fHandle < 0)
        return fHandle;

    bytes_read = read(fHandle, buf, 160);

    close(fHandle);
}

void print_usage(char *thisFile) {
    printf("\nUsage: %s [OPTION] ... [FILE]\n", thisFile);
    printf("Helper utility for configuring the si5332 PLL.\n\n");
    printf("Arguments:\n");
    printf("  -g, --get         Get the currently loaded clock configuration\n");
    printf("                    from the PLL\n");
    printf("  -s, --set [FILE]  Set the clock configuration of the PLL.\n");
    printf("                    [FILE] is a register configuration in C header\n");
    printf("                    format, as generated from Silicon Labs ClockBuilder\n");
    printf("                    Pro\n\n");
    printf("The si5332.ko kernel module must be loaded for this utility to be used.\n\n");
}

int main(int argc, char **argv) {

    int num_regs = 0, i = 0;
    char *fileName = "test.txt";
    int ret = 0;
    char * thisFile;
    bool isDefault = true;
    bool debug = false;
    
    /* Declare array of structs to hold register address/value pairs */
    si5332_revd_register_t si5332_revd_registers[MAX_NUM_REGS];
    /* Declare array of structs to hold clock output/freq pairs */
    si5332_output_config_t si5332_clocks[NUM_OUTPUTS];

    thisFile = argv[0];

    if ( (argc > 1 ) && (argc < 4 )) {
        if (!strncmp(argv[1], "-g", 2) ||  !strncmp(argv[1], "--get", 5)) {
                /* Read configuration from si5332 device via sysfs */ 
                ret = si5332_read_from_sysfs();
                if(ret < 0) {
                    printf("ERROR (%d), failed to open sysfs node for reading %s\n", ret, SYSFS_DEVICE); 
                    return ret;
                }
        } else if (!strncmp(argv[1], "-s", 2) ||  !strncmp(argv[1], "--set", 5)) {
            if (argc < 3 ) {
                printf("'set' command requires a filename or DEFAULT to be provided\n");
                print_usage(thisFile);
            } else {
                /* Get filename from command-line args */
                fileName = argv[2];
                
                if (!(strncmp(fileName, "DEFAULT", 7) == 0)) {
                    isDefault = false;      
                    /* Get config register set from file */
                    num_regs = si5332_get_config_from_file(fileName, si5332_revd_registers, si5332_clocks);
                    if(num_regs < 0) {
                        switch (num_regs) {
                            case -1:
                                printf("ERROR: failed to read file %s\n", fileName);    
                                return 1;
                            case -2:
                                printf("ERROR: failed to get number of registers to read\n");
                                return 2;
                            default:
                                printf("ERROR with file %s\n", fileName);
                        }
                    }
                } 
                
                /* Write configuration to si5332 device via sysfs */ 
                ret = si5332_write_to_sysfs(num_regs, si5332_revd_registers, si5332_clocks, isDefault);
                if(ret < 0) {
                    printf("ERROR (%d), failed to open sysfs node for writing %s\n", ret, SYSFS_DEVICE); 
                    return ret;
                }
                
                if (debug) {
                    for(i=0;i<NUM_OUTPUTS;i++) {
                        printf("%s : %s\n", si5332_clocks[i].name, si5332_clocks[i].freq);
                    }

                    for(i=0;i<num_regs;i++) {
                        printf("[%d]{ 0x%x, 0x%x },\n",
                                i, 
                                si5332_revd_registers[i].address, 
                                si5332_revd_registers[i].value
                              );
                    }
                }
            }
        } else {
            print_usage(thisFile);
        }
    } else { 
        print_usage(thisFile);
    }
    
    return 0;
}
