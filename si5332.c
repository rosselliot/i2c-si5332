/*
 * I2C master interface for programming Si5332 synthesizer
 *
 */

#include <linux/spinlock.h>
#include <linux/module.h>
#include <linux/sysfs.h>
#include <linux/slab.h>
#include <linux/init.h>
#include <linux/i2c.h>
#include <linux/of.h>
#include <linux/delay.h>
#include <linux/device.h>
#include <linux/string.h>
#include <asm/uaccess.h>
#include <stdbool.h>

/* Default register configuration:
 *
 *  {"OUT0", "100 MHz"},
 *  {"OUT1", "UNUSED"},
 *  {"OUT2", "100 MHz"},
 *  {"OUT3", "88.0519480519 MHz"},
 *  {"OUT4", "88.0519480519 MHz"},
 */  
#include <si5332_regs.h>

#define DRIVER_NAME "si5332"
#define DEVICE_NAME "si5332"

#define PREAMBLE_WAIT 300000
#define ADDR_MASK 0x00FF

struct si5332_driver_data {
    struct i2c_client *client;
    struct bin_attribute bin;
    spinlock_t status_mutex;
    u8 status;
    si5332_revd_register_t *regs;
    si5332_revd_output_config_t *clocks;
    bool readNumRegs;
    u8 numRegs;
    bool confReadInProgress;
    bool readAddr;
    u8 currRegNum;
    bool readClkName;
    bool clockReadInProgress;
    u8 currClock;
};

static int si5332_init(struct i2c_client *client, char *config)
{
    int i, n, ret, numReg;
    unsigned char addr, data;
        si5332_revd_register_t *si5332_regs;
    //si5332_revd_output_config_t si5332_clocks[NUM_OUTPUTS];
    si5332_revd_output_config_t *si5332_clocks;
    struct si5332_driver_data *si5332_data = i2c_get_clientdata(client);

    /* Select register set based on config */
    /* Sysfs input strings terminate with newlines.
       sysfs_streq will compensate for newlines when
       comparing against values without newlines     */
    if (sysfs_streq(config, "DEFAULT")) {
        si5332_regs = si5332_revd_registers;
        si5332_clocks = si5332_revd_clocks;
        numReg = SI5332_REVD_REG_CONFIG_NUM_REGS;
    } else {
        /* Use values transferred from sysfs */
        si5332_regs = si5332_data->regs;
        //si5332_regs = si5332_revd_registers;
        si5332_clocks = si5332_data->clocks;
        numReg = si5332_data->numRegs;
    }
    for(n=0; n<NUM_OUTPUTS; n++) {
        dev_info(&client->dev, "Configuring %s to %s\n", 
            si5332_clocks[n].name, si5332_clocks[n].freq);
    }

    for (i=0; i<numReg; i++) {
        /*  Register decoding:
         *
         *  Each register address is 16-bits:
         *   - 8 LSBs contain the reg address
         */
        addr = (unsigned char) si5332_regs[i].address & ADDR_MASK;
        data = si5332_regs[i].value;

        /* Perform register byte write */
        ret = i2c_smbus_write_byte_data(client, addr, data);
        if (ret) {
            dev_info(&client->dev, "Failed to write to register (0%2x)\r\n", addr);
            return ret;
        }
    }
    dev_info(&client->dev, "Wrote %d configuration registers\r\n", i);
    /* Store currently set output frequency */
    si5332_data->regs = si5332_regs;
    si5332_data->clocks = si5332_clocks;
    /* Reset configuration read counts */
    si5332_data->readNumRegs = false;
    si5332_data->numRegs = 0;
    si5332_data->confReadInProgress = false;
    si5332_data->readAddr = false;
    si5332_data->currRegNum = 0;
    si5332_data->readClkName = false;
    si5332_data->clockReadInProgress = false;
    si5332_data->currClock = 0;
    
    return 0;
}

static ssize_t si5332_bin_read(struct file *filp, struct kobject *kobj,
    struct bin_attribute *attr, char *buf, loff_t off, size_t count)
{
    /* Exporting data to the user space is not supported, but this call can be
     * used for printing current status of the device */
    struct si5332_driver_data *si5332_data;
    int n = 0;
	si5332_data = dev_get_drvdata(container_of(kobj, struct device, kobj));

    dev_info(&si5332_data->client->dev,
            "Current clock configuration:\n");
    for(n=0; n<NUM_OUTPUTS; n++) {
        dev_info(&si5332_data->client->dev,
            "%s @ %s\n", 
            si5332_data->clocks[n].name, 
            si5332_data->clocks[n].freq);
    }
    return 0;
}

static ssize_t si5332_bin_write(struct file *filp, struct kobject *kobj,
    struct bin_attribute *attr, char *buf, loff_t off, size_t count)
{
    /* store large buffers in global data (static), to prevent overloading the stack */
    static si5332_revd_register_t si5332_regs[MAX_NUM_REGS];
    static si5332_revd_output_config_t si5332_clocks[NUM_OUTPUTS];
    struct si5332_driver_data *si5332_data;
    long num_regs = -1;
    long regValue = 0;
    int ret = 0;
    char *newline;
    bool ready = false;

	si5332_data = dev_get_drvdata(container_of(kobj, struct device, kobj));
    
    /* strip newline from sysfs input string */
    if ((newline=strchr(buf, '\n')) != NULL)
        *newline = '\0';
    
    if (count > 5)
    {
        dev_err(&si5332_data->client->dev, "Input limited to 5 bytes \n");
    } else {

        /* DEBUG: print all current values in data structure */    
        /* dev_info(&si5332_data->client->dev,
                "numRegs = %d\nconfReadInProgess =%d\nclockReadInProgess =%d\ncurrRegNum=%d\ncurrClock =%d\n",
                si5332_data->numRegs,
                si5332_data->confReadInProgress,   
                si5332_data->clockReadInProgress,   
                si5332_data->currRegNum,   
                si5332_data->currClock);
        */
        if (!strncmp(buf, "DEFAULT", 7)) {
            /* Set default configuration from included header file */
            dev_info(&si5332_data->client->dev, "Setting default configuration\n");
            si5332_init(si5332_data->client, "DEFAULT");
            si5332_data->readNumRegs = false;
            si5332_data->numRegs = 0;
            si5332_data->confReadInProgress = false;
            si5332_data->readAddr = false;
            si5332_data->currRegNum = 0;
            si5332_data->readClkName = false;
            si5332_data->clockReadInProgress = false;
            si5332_data->currClock = 0;
            return count;
        } else if((!si5332_data->clockReadInProgress) && (!si5332_data->confReadInProgress)) {
            /* We are not currently reading a configuration */
            if (!strncmp(buf, "CUSTOM", 6)) {
                /* Read register values from the sysfs interface */
                si5332_data->confReadInProgress = true;
                dev_info(&si5332_data->client->dev, "Begin read of custom register configuration\n");
            } else {
                dev_err(&si5332_data->client->dev, "Unknown input '%s'\n", buf);
            }
        } else {
            /* A configuration transfer is in progress */
            if (si5332_data->confReadInProgress) {
                 if (si5332_data->readNumRegs == false) {
                    ret = kstrtol(buf, 10, &num_regs);
                    if (ret < 0) {
                        dev_err(&si5332_data->client->dev, "(%d)Unable to parse number of registers from input: '%s'\n", ret, buf);
                        si5332_data->confReadInProgress = false;
                    } else {
                        /* First value is number of regs to write */
                        si5332_data->numRegs = (u8)num_regs;
                        si5332_data->readNumRegs = true;
                    }
                } else {
                    ret = kstrtol(buf, 16, &regValue);
                    if (ret < 0) {
                        dev_err(&si5332_data->client->dev, "(%d)Unable to parse register address/value from input: '%s'\n", ret, buf);
                        si5332_data->confReadInProgress = false;
                    }
                    if (!si5332_data->readAddr) {
                        si5332_regs[si5332_data->currRegNum].address =
                            (unsigned int)regValue;
                        si5332_data->readAddr = true;
                    } else {
                        si5332_regs[si5332_data->currRegNum].value =
                            (unsigned char)regValue;
                        si5332_data->readAddr = false;
                        si5332_data->currRegNum++;
                    }
 
                    if (si5332_data->currRegNum == si5332_data->numRegs) {
                        /* We have read all registers */
                        si5332_data->confReadInProgress = false;
                        si5332_data->clockReadInProgress = true;
                    }
                }        
            } else if (si5332_data->clockReadInProgress) {
                
                if (!si5332_data->readClkName) {
                    si5332_clocks[si5332_data->currClock].name = kstrdup(buf, GFP_KERNEL);
                    si5332_data->readClkName = true;
                } else {
                    si5332_clocks[si5332_data->currClock].freq = kstrdup(buf, GFP_KERNEL);
                    si5332_data->readClkName = false;
                    si5332_data->currClock++;
                }
                
                if (si5332_data->currClock == NUM_OUTPUTS) {
                    si5332_data->clockReadInProgress = false;
                    ready = true;
                }
            }
       }    
    }
    /* Store transferred values in module data object */
    si5332_data->regs   = si5332_regs;
    si5332_data->clocks = si5332_clocks;
    
    if (ready) {
        /* All data has been transferred. Configure PLL */
        dev_info(&si5332_data->client->dev, "All data transferred. Initialising PLL...\n"); 
        si5332_init(si5332_data->client, "CUSTOM");
    }        
	return count;
}

static int si5332_i2c_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
    struct si5332_driver_data *si5332_data;
    int ret;

    dev_info(&client->dev, "Initialization\n");

    /* Allocate kernel memory for the device structure */
    si5332_data = devm_kzalloc(&client->dev, sizeof(struct si5332_driver_data), GFP_KERNEL);
    if (!si5332_data) {
        dev_info(&client->dev, "EEROR: Unable to allocate kernel memory for device\r\n");
        return -ENOMEM;
    }
    si5332_data->client = client;
    /* Initiaze the mutex */
    spin_lock_init(&si5332_data->status_mutex);

    /* Set device flag to master */
    i2c_set_clientdata(client, si5332_data);

    /* Sysfs api for accesing the device from the userspace */
    sysfs_bin_attr_init(&si5332_data->bin);
    si5332_data->bin.attr.name = DEVICE_NAME;
    si5332_data->bin.attr.mode = S_IRUSR | S_IWUSR;
    si5332_data->bin.write = si5332_bin_write;
    si5332_data->bin.read = si5332_bin_read;
    si5332_data->bin.size = 5;

    ret = sysfs_create_bin_file(&client->dev.kobj, &si5332_data->bin);
    if (ret)
        return ret;

    /* Get device version */
    dev_info(&client->dev, "Part number is: %2x rev. %2x grade %2x\r\n",
                i2c_smbus_read_byte_data(client, 0x0D),
                i2c_smbus_read_byte_data(client, 0x0E),
                i2c_smbus_read_byte_data(client, 0x0F));

    /* Initialize output clocks using default configuration */
    ret = si5332_init(client, "DEFAULT");
    if (ret) {
        dev_info(&client->dev, "Failed to initialize output clock\r\n");
        return ret;
    }
    return 0;
}

static int si5332_i2c_remove(struct i2c_client *client)
{
    struct si5332_driver_data *si5332_data = i2c_get_clientdata(client);

    sysfs_remove_bin_file(&client->dev.kobj, &si5332_data->bin);
    devm_kfree(&client->dev, si5332_data);
    dev_info(&client->dev, "Removed\r\n");

    return 0;
}

static const struct i2c_device_id si5332_i2c_id[] = {
    { DEVICE_NAME, 0 },
    { }
};
MODULE_DEVICE_TABLE(i2c, si5332_i2c_id);

static struct i2c_driver si5332_i2c_driver = {
    .driver = {
        .name = DRIVER_NAME,
    },
    .probe = si5332_i2c_probe,
    .remove = si5332_i2c_remove,
    .id_table = si5332_i2c_id,
};
module_i2c_driver(si5332_i2c_driver);

MODULE_DESCRIPTION("Interface for configuring Si5332 Digital PLL Clock Synthesizer");
MODULE_LICENSE("GPL v2");
